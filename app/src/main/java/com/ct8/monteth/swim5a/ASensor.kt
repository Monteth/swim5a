package com.ct8.monteth.swim5a

import android.annotation.SuppressLint
import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_asensor.*

class ASensor : AppCompatActivity(), SensorEventListener {
    private lateinit var sensorManager: SensorManager
    private var sensorType = 0

    @SuppressLint("SetTextI18n")
    override fun onSensorChanged(event: SensorEvent?) {
        val stringBuilder = StringBuilder()
        when(sensorType){
            Sensor.TYPE_LIGHT->{
                stringBuilder.append("Ambient light level: ")
                stringBuilder.append(event?.values?.get(0))
                stringBuilder.append(" lux")
            }
            Sensor.TYPE_ACCELEROMETER->{
                stringBuilder.append("X acceleration: ")
                stringBuilder.append(String.format("%7.4f", event?.values?.get(0)))
                stringBuilder.append(" m/s\u00B2\nY acceleration: ")
                stringBuilder.append(String.format("%7.4f", event?.values?.get(1)))
                stringBuilder.append(" m/s\u00B2\nZ acceleration: ")
                stringBuilder.append(String.format("%7.4f", event?.values?.get(2)))
                stringBuilder.append(" m/s\u00B2")
            }
        }
        acAsensor_tvData.text = stringBuilder

        acAsensor_tvStatus.text = "Accuracy: " + when(event?.accuracy){
            1-> "Low"
            2-> "Medium"
            3-> "High"
            else -> "Low"
        }
    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_asensor)
        sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager

        sensorType = intent.getIntExtra("sensorType", Sensor.TYPE_LIGHT)
        when(sensorType){
            Sensor.TYPE_LIGHT->{
                title = "Light status"
            }
            Sensor.TYPE_ACCELEROMETER->{
                title = "Accelerometer status"
            }
        }
    }

    override fun onResume() {
        super.onResume()

        val sensor: Sensor? = sensorManager.getSensorList(sensorType)?.get(0)
        sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL)
    }

    override fun onPause() {
        super.onPause()
        sensorManager.unregisterListener(this)
    }
}
