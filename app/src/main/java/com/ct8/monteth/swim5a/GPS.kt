package com.ct8.monteth.swim5a

import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_gps.*

class GPS : AppCompatActivity(), LocationListener {
    private lateinit var locationManager: LocationManager
    val REQUEST_LOCATION = 123

    override fun onLocationChanged(location: Location?) {
        val stringBuilder = StringBuilder()
        if (location != null) {
            stringBuilder.append("Altitude: ")
            stringBuilder.append(location.altitude)
            stringBuilder.append("m\nBearing: ")
            stringBuilder.append(location.bearing)
            stringBuilder.append("\u00B0\nLatitude: ")
            stringBuilder.append(location.latitude)
            stringBuilder.append("\nLongitude: ")
            stringBuilder.append(location.longitude)
            stringBuilder.append("\nSpeed: ")
            stringBuilder.append(location.speed)
            stringBuilder.append("m/s")
            acGps_tvData.text = stringBuilder

            val stringBuilder2 = StringBuilder()
            stringBuilder2.append("Accuracy: ")
            stringBuilder2.append(location.accuracy)
            stringBuilder2.append("m")
            acGps_tvStatus.text = stringBuilder2
        }
    }

    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {}

    override fun onProviderEnabled(provider: String?) {}

    override fun onProviderDisabled(provider: String?) {
        if (LocationManager.GPS_PROVIDER.contentEquals(provider!!))
            finish()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gps)

        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        acGps_tvData.text = "Czekam na dane GPS..."
    }

    override fun onResume() {
        super.onResume()

        if (ContextCompat.checkSelfPermission(this,
                        android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            acGps_tvPermission.text = "REQUESTING PERMISSION"
            ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION), REQUEST_LOCATION)
        }
        requestLocationUpdates()
    }

    private fun requestLocationUpdates() {
        if (ContextCompat.checkSelfPermission(this,
                        android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            acGps_tvPermission.text = "PERMISSION GRANDTED"
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 10.0f, this)
        }else{
            acGps_tvPermission.text = "PERMISSION DENIED"
        }
    }

    override fun onPause() {
        super.onPause()

        if (ContextCompat.checkSelfPermission(this,
                        android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            locationManager.removeUpdates(this)
        }
    }
}
