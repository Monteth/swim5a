package com.ct8.monteth.swim5a

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.hardware.Sensor
import android.hardware.SensorManager
import android.location.LocationManager
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onResume() {
        super.onResume()

        val sensorManager: SensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager

        if (sensorManager.getSensorList(Sensor.TYPE_LIGHT).isNotEmpty()){
            acMain_tv1.text = "Light sensor is enabled"
            acMain_tv1.setTextColor(Color.GREEN)
            acMain_bt1.isEnabled = true
        }else{
            acMain_tv1.text = "Light sensor is disabled"
            acMain_tv1.setTextColor(Color.RED)
            acMain_bt1.isEnabled = false
        }

        if (sensorManager.getSensorList(Sensor.TYPE_ACCELEROMETER).isNotEmpty()){
            acMain_tv2.text = "Accelerometer is enabled"
            acMain_tv2.setTextColor(Color.GREEN)
            acMain_bt2.isEnabled = true
        }else{
            acMain_tv2.text = "Accelerometer is disabled"
            acMain_tv2.setTextColor(Color.RED)
            acMain_bt2.isEnabled = false
        }

        val locationManager:LocationManager = getSystemService(LOCATION_SERVICE) as LocationManager

        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
            acMain_tv3.text = "GPS is enabled"
            acMain_tv3.setTextColor(Color.GREEN)
            acMain_bt3.isEnabled = true
        }else{
            acMain_tv3.text = "GPS is disabled"
            acMain_tv3.setTextColor(Color.RED)
            acMain_bt3.isEnabled = false
        }

    }

    fun startAktywnosc(view:View){
        var intent : Intent = Intent()
        when(view){
            acMain_bt3->{
                intent = Intent(this, GPS::class.java)
            }
            acMain_bt1->{
                intent = Intent(this, ASensor::class.java)
                intent.putExtra("sensorType", Sensor.TYPE_LIGHT)
            }
            acMain_bt2->{
                intent = Intent(this, ASensor::class.java)
                intent.putExtra("sensorType", Sensor.TYPE_ACCELEROMETER)
            }
        }
        startActivity(intent)
    }
}
